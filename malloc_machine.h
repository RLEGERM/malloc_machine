/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc_machine.h                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rleger <rleger@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/03/08 14:46:19 by rleger            #+#    #+#             */
/*   Updated: 2023/03/08 14:47:44 by rleger           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MALLOC_MACHNE_H
# define MALLOC_MACHINE_H

#include <stdlib.h>

typedef struct s_mem
{
	void			*data;
	struct s_mem	*next;
}					t_mem;

#endif